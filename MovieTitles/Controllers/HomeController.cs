﻿using System;
using System.Web.Mvc;
using log4net;
using MovieTitles.Helper;
using MovieTitles.Models;
using MovieTitles.Repositry;

namespace MovieTitles.Controllers
{

    public class HomeController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HomeController));
        Repository _repository = new Repository();
        
        /// <summary>
        /// Get the Basic Information of Movie
        /// </summary>
        /// <returns></returns>
        public ActionResult MovieInfo()
        {
            try
            {
                Logger.Info("Invoked Movie Info controller");
                var _moviesBasicInfo = new MoviesList();
                _moviesBasicInfo.MovieBasicsList = _repository.GetMovies();
                return View(_moviesBasicInfo);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in MovieInfo :", ex);        
                return View("Error");
            }

            
        }

        /// <summary>
        /// Get the details of the Movie for the given Title Id
        /// </summary>
        /// <param name="titleId">Long Title Id</param>
        /// <returns></returns>
        public ActionResult GetMovieDetails(long titleId)
        {
            try
            {
                Logger.Info(string.Format("Invoked GetMovieDetails controller with TitleId :{0}", titleId));
                var movieInfoList = new MovieInfoList();
                movieInfoList.MovieInfo = _repository.GetMovieDetais(titleId);
                return View("PopupTable", movieInfoList);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetMovieDetails :", ex);
                return View("Error");   
            }
                       
        }
    }
}
