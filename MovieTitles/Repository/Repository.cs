﻿using System;
using System.Collections.Generic;
using System.Linq;
using MovieTitles.Helper;
using MovieTitles.Models;


namespace MovieTitles.Repositry
{
    /// <summary>
    /// Class contains the method to connect
    /// with DB and retrieve details
    /// </summary>
    public class Repository
    {
        /// <summary>
        /// Get the Basic Information of Movie
        /// </summary>
        /// <returns>List of Movies Basic Information
        /// </returns>
        public List<MoviesBasicInfo> GetMovies()
        {
            List<MoviesBasicInfo> movieInfo = null;
            try
            {
                Logger.Info("Invoked GetMovies");
                using (var movieEntities = new TitlesEntities())
                {
                    movieInfo = (from movieinfo in movieEntities.Titles
                                 select new MoviesBasicInfo
                                 {
                                     TitleId = movieinfo.TitleId,
                                     MovieName = movieinfo.TitleName,
                                     AwardWon = movieinfo.Awards.Count() > 0 ? true : false,
                                     GenreName = movieinfo.TitleGenres.Count() > 1 ? "Multiple" : "Single",
                                     ReleaseYear = movieinfo.ReleaseYear,
                                     IsOnScreen = movieinfo.TitleParticipants.FirstOrDefault().IsOnScreen,
                                 }).Distinct().ToList();

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in MovieInfo :", ex);     
                throw ex;
            }

            return movieInfo;

        }

        /// <summary>
        /// Get the Movie details for the given Title Id
        /// </summary>
        /// <param name="titleId">Long Title Id</param>
        /// <returns>Information of the Movie</returns>
        public List<DetailedMovieInfo> GetMovieDetais(long titleId)
        {
            List<DetailedMovieInfo> movieInfo = null;
            try
            {
                Logger.Info(string.Format("Invoked GetMovieDetails with TitleId :{0}", titleId));

                using (var movieEntities = new TitlesEntities())
                {

                    movieInfo = (from movieinfo in movieEntities.Titles.AsEnumerable()                                                                                                                                                    
                                 where movieinfo.TitleId == titleId
                                 select new DetailedMovieInfo
                                 { 
                                     Awards = movieinfo.Awards.ToList(),
                                     TitleParticipants= movieinfo.TitleParticipants.ToList(),   
                                     StoryLines = movieinfo.StoryLines.ToList(),                                    
                                     OtherNames = movieinfo.OtherNames.ToList(),
                                 }).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetMovieDetais :", ex); 
                throw ex;
            }

            return movieInfo;
        }
    }
}