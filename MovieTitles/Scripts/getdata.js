﻿$(document).ready(function () {
    titleClick();  

    $('#foot').css({ 'top': $(window).height() -70 });
});

function closeClick() {
    $('.close').unbind('click');
    $('.close').click(function () {
        titleClick();
    });
}


function titleClick() {
    $('.movieName').unbind('click');
    $('.movieName').click(function (e) {
        if ($(this).find('.grid-header-title').length == 0) {
            var text = $(this).parent().find('td.titleId').text();
            if (text != "") {
                $.get('/Home/GetMovieDetails', { titleId: text }, function (data) {
                    $('#popupDataContainer').html(data);
                    $('#tablePopup').modal({ keyboard: false });
                    closeClick();
                });
            }
        }
    });
}


