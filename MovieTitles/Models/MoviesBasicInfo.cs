﻿using System.Collections.Generic;

namespace MovieTitles.Models
{
    /// <summary>
    /// Class contains the list of
    /// movies basic information
    /// </summary>
    public class MoviesList
    {
        public MoviesList()
        {
            MovieBasicsList = new List<MoviesBasicInfo>();
        }

        public List<MoviesBasicInfo> MovieBasicsList { get; set; } 
    }

    /// <summary>
    /// Class contains the basic
    /// information members of the
    /// Movies
    /// </summary>
    public class MoviesBasicInfo
    {
        public int TitleId { get; set; }
        public string MovieName { get; set; }
        public int? ReleaseYear { get; set; }      
        public bool AwardWon { get; set; }
        public string GenreName { get; set; }
        public bool IsOnScreen { get; set; }

    }
   
       
    
}