﻿

using System.Collections.Generic;
using System.Data.Objects.DataClasses;
namespace MovieTitles.Models
{
    /// <summary>
    /// Class contains the list of
    /// movies basic information
    /// </summary>
    public class MovieInfoList
    {
        public MovieInfoList()
        {
            MovieInfo = new List<DetailedMovieInfo>();
        }

        public List<DetailedMovieInfo> MovieInfo { get; set; }
    }
    /// <summary>
    /// Class contains the 
    /// information members of the movie
    /// </summary>
    public class DetailedMovieInfo
    {
        public List<Award> Awards { get; set; }
        public List<TitleParticipant> TitleParticipants { get; set; }
        public List<StoryLine> StoryLines { get; set; }
        public List<OtherName> OtherNames { get; set; }
    }

}